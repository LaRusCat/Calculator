#include "Calculator.h"

#include <algorithm>

double Calculator::calculate(const string& expression)
{
    tokenize(expression);
    m_current_token = m_tokens.front();
    return process_plus_minus();
}

void Calculator::tokenize(const string& expression)
{
    std::string number;
    for (auto& character : expression) {
        if (isdigit(character) || character == '.') {
            number += character;
        } else {
            push_number_token(number);
            m_tokens.push_back(string(1, character));
        }
    }
    push_number_token(number);
}

void Calculator::push_number_token(string& number)
{
    if (!number.empty()) {
        if (count(number.begin(), number.end(), '.') > 1) {
            throw runtime_error("Too much . in number");
        }
        m_tokens.push_back(number);
        number.clear();
    }
}

void Calculator::next_token()
{
    m_tokens.pop_front();

    if (!m_tokens.empty()) {
        m_current_token = m_tokens.front();
    } else {
        m_current_token = "";
    }
}

double Calculator::process_plus_minus()
{
    double result = process_mul_div();

    while (m_current_token == "+" || m_current_token == "-") {
        if (m_current_token == "+") {
            next_token();
            result += process_mul_div();
        }
        if (m_current_token == "-") {
            next_token();
            result -= process_mul_div();
        }
    }

    return result;
}

double Calculator::process_mul_div()
{
    double result = process_parenthesis_numbers();

    while (m_current_token == "*" || m_current_token == "/") {
        if (m_current_token == "*") {
            next_token();
            result *= process_parenthesis_numbers();
        }
        if (m_current_token == "/") {
            next_token();
            double denominator = process_parenthesis_numbers();
            result /= denominator;
        }
    }

    return result;
}

/**
 *  There are some issues with incorrect parenthesis handling
 */
double Calculator::process_parenthesis_numbers()
{
    double result = 0.0;

    if (m_current_token == "-") {
        next_token();
        result = -process_parenthesis_numbers();
    } else if (m_current_token == "(") {
        next_token();
        result = process_plus_minus();
        if (m_current_token != ")") {
            throw runtime_error("Incorrect parenthesis");
        }
        next_token();
    } else {
        if (m_current_token == "+") {
            next_token();
            if (m_current_token == "+") {
                throw runtime_error("Too much +");
            }
        }
        try {
            result = stod(m_current_token);
        } catch (const invalid_argument&) {
            throw runtime_error("Invalid expression");
        }
        next_token();
    }

    return result;
}
