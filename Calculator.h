#pragma once

#include <list>
#include <string>

using namespace std;

class Calculator {
public:
    double calculate(const string& expression);

private:
    void tokenize(const string& expression);
    void push_number_token(string& number);
    void next_token();
    double process_plus_minus();
    double process_mul_div();
    double process_parenthesis_numbers();

    list<string> m_tokens;
    string m_current_token;
};
